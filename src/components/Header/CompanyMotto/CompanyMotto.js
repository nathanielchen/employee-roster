import React from "react";
import "./CompanyMotto.css";

const CompanyMotto = props => (
  <div className="company_motto" style={{ height: props.height }}>
    Since {props.companyMotto}
  </div>
);

export default CompanyMotto;
