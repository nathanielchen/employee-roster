import React from "react";
import "./CompanyName.css";
import logo from "../assets/logo.png";

const CompanyName = props => (
  <div className="row middle-xs" style={{ height: props.height }}>
    <img className="logo" src={logo} alt="" />
    <span className="company_name">{props.companyName}</span>
  </div>
);

export default CompanyName;
