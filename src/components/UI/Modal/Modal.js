import React, { Component } from "react";

import classes from "./Modal.css";
import Aux from "../../../hoc/Aux";
import Backdrop from "../Backdrop/Backdrop";

class Modal extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.show !== this.props.show ||
      nextProps.children !== this.props.children
    );
  }

  render() {
    return (
      <Aux>
        <Backdrop show={this.props.show} clicked={this.props.modalClosed} />

        <div
          className="modal"
          style={{
            transform: this.props.show ? "translateY(0)" : "translateY(-100vh)",
            opacity: this.props.show ? "1" : "0"
          }}
        >
          <button onClick={this.props.modalClosed} className="close">
            <svg className="close_svg" viewbox="0 0 10 10">
              <path d="M0,0 L10,10 M10,0 L0,10" />
            </svg>
          </button>
          {this.props.children}
        </div>
      </Aux>
    );
  }
}

export default Modal;
