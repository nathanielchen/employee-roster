import React, { Component } from "react";
import PropTypes from "prop-types";
import Employees from "./Employees";
import classes from "./index.css";
import Employee from "./Employee/Employee";
import Modal from "../../components/UI/Modal/Modal";

class OurEmployees extends Component<Props> {
  constructor(props: Props) {
    super(props);

    this.state = {
      isViewingModal: false,
      selectedEmployee: null
    };
  }

  viewingModalHandler = employee => {
    this.setState({
      isViewingModal: true,
      selectedEmployee: employee
    });
  };

  closeModalHandler = () => {
    this.setState({ isViewingModal: false });
  };

  render() {
    let modalView = null;
    console.log("sdfasdfsd f");
    if (this.state.selectedEmployee !== null) {
      console.log("sdfasdfsd f", this.state.selectedEmployee);
      modalView = (
        <Modal
          show={this.state.isViewingModal}
          modalClosed={this.closeModalHandler}
        >
          <Employee
            truncateAtWordLight={2000}
            fullView={true}
            {...this.state.selectedEmployee}
          />
        </Modal>
      );
    }

    return (
      <div className="page start-xs wrap container-fluid">
        <h2 className="page_title">Our Employees</h2>
        <hr />
        <Employees viewingModalHandler={this.viewingModalHandler} />
        {modalView}
      </div>
    );
  }
}
export default OurEmployees;
