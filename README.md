# Employee Roster

This app started from [`WDP UI code test`](https://github.com/facebookincubator/create-react-app).

# Quickstart

Install [NVM](https://github.com/creationix/nvm) and then run,

    nvm install

Then,

    yarn install

    yarn build

    yarn start

## Helping diagram images

Please find the system structure diagram for the overview of the system.

CodeText application system structure diagram.jpg
hand drawn system structure-UML.jpg

## This code requires a internet connection to operate

    API to Firebase
    https://nate-277a7.firebaseio.com/.json

    Image accest from S3
    https://s3.amazonaws.com/uifaces/faces/twitter/derekebradley/

## Style (CSS)

## The Application Requirements

This simple employee roster app will use a static sample-data.json file with randomly generated sample data that should be used to create a simple one-page app of the roster for the company's employees.

The roster should be represented in a "card" layout (see wireframe grid-view.png) which initially shows minimal employee information consisting of the employee's name, avatar picture and a truncated excerpt of their bio.
