import React, { Component } from "react";
import "./Employee.css";
import { displayDate, truncateAtWordBoundary } from "../../../shared/utility";

type Props = {
  fullView: false,
  id: number,
  avatar: string,
  firstName: string,
  lastName: string,
  jobTitle: string,
  age: number,
  bio: string,
  dateJoined: Date,
  isSelected: boolean,
  truncateAtWordLight: Number
};

class Employee extends Component<Props> {
  constructor(props: Props) {
    super(props);

    this.state = {
      errored: false,
      selected: false
    };
  }

  handleError = event => {
    if (!this.props.fullView) this.setState({ errored: true });
  };

  selectedHandler = event => {
    if (!this.props.fullView) this.setState({ selected: true });
  };

  onClickHandler = () => {
    if (!this.props.fullView)
      this.props.viewingModalHandler(this.props.employee);
  };

  deselectedHandler = event => {
    this.setState({ selected: false });
  };

  render() {
    let employeeInfo = null;

    employeeInfo = (
      <div className="card__info">
        <label className="card__job-title">{this.props.jobTitle}</label>
        <label className="card__label">{this.props.age}</label>
        <label className="card__label">
          {displayDate(this.props.dateJoined)}
        </label>
      </div>
    );

    if (this.state.errored) {
      return null;
    } else {
      return (
        <div
          className={`card_wrapper col-xs-12${
            this.props.fullView
              ? " fullview-card_wrapper"
              : " col-sm-6 col-md-4"
          }`}
          onClick={this.onClickHandler}
          onMouseOver={this.selectedHandler}
          onMouseLeave={this.deselectedHandler}
        >
          <article
            className={`card row middle-xs no-margain${
              this.state.selected ? " selected" : ""
            }`}
          >
            <div className="col-xs-12 col-sm-6">
              <span className="card__image row center-xs">
                <img
                  className="card__avatar"
                  onError={this.handleError}
                  src={this.props.avatar}
                  alt={`${this.props.firstName} ${this.props.lastName}`}
                />
              </span>
              {employeeInfo}
            </div>

            <div className="col-xs-12 col-sm-6 right-plan">
              <div className="card__name">{`${this.props.firstName} ${
                this.props.lastName
              }`}</div>
              <div>
                <p className="card__bio">
                  {truncateAtWordBoundary(
                    this.props.bio,
                    this.props.truncateAtWordLight
                  )}
                </p>
              </div>
            </div>
          </article>
        </div>
      );
    }
  }
}

export default Employee;

Employee.defaultProps = {
  truncateAtWordLight: 150
};
