export const HIDE_MODAL = "HIDE_MODAL";
export const SHOW_MODAL = "SHOW_MODAL";

export const SET_COMPANYINFO = "SET_COMPANYINFO";
export const SET_EMPLOYEES = "SET_EMPLOYEES";
export const FETCH_DATA_FAILED = "FETCH_DATA_FAILED";
