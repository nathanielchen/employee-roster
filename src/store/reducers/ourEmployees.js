import * as actionTypes from "../actions/actionTypes";

const initialState = {
  employees: null,
  companyInfo: null,
  isModal: false,
  error: false
};

export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  };
};

const setEmployees = (state, action) => {
  return updateObject(state, {
    employees: [...action.employees],
    error: false
  });
};

const setCompanyInfo = (state, action) => {
  return updateObject(state, {
    companyInfo: { ...action.companyInfo },
    error: false
  });
};

const fetchDateFailed = (state, action) => {
  return updateObject(state, { error: true });
};

const showModal = (state, action) => {
  return updateObject(state, {
    hasModal: true
  });
};

const hideModal = state => {
  return updateObject(state, {
    hasModal: false
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_COMPANYINFO:
      return setCompanyInfo(state, action);
    case actionTypes.SET_EMPLOYEES:
      return setEmployees(state, action);
    case actionTypes.FETCH_DATA_FAILED:
      return fetchDateFailed(state, action);
    case actionTypes.SHOW_MODAL:
      return showModal(state);
    case actionTypes.HIDE_MODAL:
      return hideModal(state);
    default:
      return state;
  }
};

export default reducer;
