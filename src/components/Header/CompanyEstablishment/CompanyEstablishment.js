import React from "react";
import "./CompanyEstablishment.css";
import { displayDate } from "../../../shared/utility";

const CompanyEstablishment = props => (
  <div
    className="company_establishment col-xs-12 "
    style={{ height: props.height }}
  >
    Since {displayDate(props.companyEst)}
  </div>
);

export default CompanyEstablishment;
