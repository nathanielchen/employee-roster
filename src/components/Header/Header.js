import React, { Component } from "react";
import { connect } from "react-redux";
import errorHandlerHOC from "../../hoc/ErrorHandlerHOC";
import axios from "../../axios-orders";
import "./Header.css";
import CompanyName from "./CompanyName/CompanyName";
import CompanyEstablishment from "./CompanyEstablishment/CompanyEstablishment";
import CompanyMotto from "./CompanyMotto/CompanyMotto";

class Header extends Component<Props> {
  render() {
    let headerView = null;
    if (this.props.companyInfo) {
      headerView = (
        <header className="header row start-xs wrap container-fluid">
          <div className="col-xs-9 ol-sm-6 start-sm">
            <CompanyName companyName={this.props.companyInfo.companyName} />
            <CompanyMotto companyMotto={this.props.companyInfo.companyMotto} />
          </div>
          <div className="col-xs-3 ol-sm-6 end-xs row bottom-xs no-margain no-padding hide-xs show-sm">
            <CompanyEstablishment
              companyEst={this.props.companyInfo.companyEst}
            />
          </div>
        </header>
      );
    }
    return headerView;
  }
}

const mapStateToProps = state => {
  return {
    companyInfo: state.ourEmployees.companyInfo
  };
};

export default connect(mapStateToProps)(errorHandlerHOC(Header, axios));
